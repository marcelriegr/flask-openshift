from setuptools import setup

setup(name='App',
      version='1.0',
      description='Blim App',
      author='Marcel Rieger',
      author_email='marcel@blim.us',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask>=0.7.2', 'MarkupSafe'],
     )
